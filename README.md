# Star Trek Adventures

<a href='https://ko-fi.com/fantasticmrpanda' target='_blank'><img height='36' style='border:0px;height:36px;' src='https://cdn.ko-fi.com/cdn/kofi2.png?v=2' border='0' alt='¡Apóyame en Ko-Fi!' /></a>

 
Ficha y sistema NO OFICIAL de **Star Trek Adventures** para Foundry VTT usando [Sandbox](https://gitlab.com/rolnl/sandbox-system-builder/) de [Seregras](https://www.youtube.com/c/RolNL/).

**Requisitos mínimos**: Sandbox System Builder versión 0.3.9 o mayor. 

Considera apoyar el [Patreon](https://www.patreon.com/seregras) de Seregras autor de Sandbox y contribuye a mejoras y ampliaciones del sistema.

Instalación recomendada usando este enlace [world.json](https://gitlab.com/jmpalacios/sta/-/raw/master/world.json) desde **Game Worlds** y poniendo el enlace en *Manifest Url*.

Espero que lo disfrutéis.